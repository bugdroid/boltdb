var fs = require("fs");

exports.update = function(db, key, value) {
	var jsonFile = fs.readFileSync(db, "utf-8");
	jsonFile = JSON.parse(jsonFile);
	eval('jsonFile.' + key + '="' + value + '"');
	jsonFile = JSON.stringify(jsonFile, null);
	fs.writeFile(db, jsonFile, function(err) {
		if(err) {
			return err;
		} else {
			return "Database " + db + " updated";
		}
	});
}

exports.get = function(db, key) {
	var jsonFile = fs.readFileSync(db, "utf-8");
	jsonFile = JSON.parse(jsonFile);
	var value = eval("jsonFile." + key);
	return value;
}
